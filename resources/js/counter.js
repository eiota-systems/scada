class Counter {
	constructor(parent) {
		this.parent = document.getElementById(parent);
    const height = this.parent.offsetHeight;
    this.parent.setAttribute("style", "max-height: "+height+"px");
		this.parent.innerHTML = 0;
		this.number = 0;
    this.next = 0;
    this.dir = 1;
    this.running = false;
	}

	render(num) {
		this.next = Math.trunc(num);
		this.dir = this.next > this.number ? 1 : -1;
		const animate = (ts) => {
      this.running = true;
			if(Math.trunc(Math.floor(this.number - this.next)) != 0){
			  this.number+= this.dir;
			  this.parent.innerHTML = "<span>" + this.number + "</span>";
      }
      requestAnimationFrame(animate);
		}
    if(!this.running) requestAnimationFrame(animate);
	}
}
