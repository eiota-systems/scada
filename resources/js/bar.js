class Bar{
  constructor(data, color_pallete, bar_width, max_domain, parent){
    this.bar_width = bar_width;
    this.max_domain = max_domain;
		this.color_pallete = color_pallete;
    this.parent = document.getElementById(parent);
    const width = this.parent.offsetWidth;
    const height = this.parent.offsetHeight - 5;
		this.height = height;
		this.xScale = d3.scaleBand()
    	.domain(d3.range(data.length))
      .rangeRound([0, width])
      .paddingInner(0.05);

		this.yScale = d3.scaleLinear()
   		.domain([0, max_domain])
      .range([0, height]);

    this.colorScale = d3.scaleLinear()
   		.domain([0, max_domain]);

    this.color = val => this.color_pallete(this.colorScale(val));

		this.svg = d3.create("svg")
			.attr("style", "max-height: "+height+"px;")
      .attr("viewBox", [0, 0, width, height])
      .style("overflow", "hidden");
		
		const self = this;
		this.svg.selectAll("rect")
      .data(data)
      .enter()
      .append("rect")
      .attr("x", function(d, i) { // position in x-axis
            return self.xScale(i); // we will pass the values from the dataset
          })
      .attr("y", function(d) {
            return height - self.yScale(d);
          })
      .attr("width", this.bar_width) //Asks for the bandwith of the scale
      .attr("height", function(d) {
            return self.yScale(d);
          })
      .attr("fill", d => this.color(d));

		this.parent.appendChild(this.svg.node());
  }

  render(data){
		this.svg.selectAll("rect")
    	.data(data)
      .transition()
      .duration(200)
      .attr("y", d => this.height - this.yScale(d))
      .attr("height", d => this.yScale(d))
      .style("fill", d => this.color(d));
  }
}
