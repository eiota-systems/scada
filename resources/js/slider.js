class Slider{
  constructor(range, parent, oninput, vertical = true){
    this.parent = document.getElementById(parent);
		this.parent.setAttribute("style", "position: relative;");
		this.height = this.parent.offsetHeight;
		this.width = this.parent.offsetWidth;
		this.range = range[1] -  range[0];
		this.range_list = range;
		this.vertical = vertical;
		this.oninput = oninput;
		this.y = 0;
		this.value = range[0];
		this.next = 0;
		this.running = false;

    this.container = document.createElement("div");
    this.container.classList.add("slider");
    if(!vertical) this.container.classList.add("horizontal");

		this.background = document.createElement("div");
		this.background.classList.add("background");
		
    this.slider = document.createElement("div");
    this.slider.classList.add("thumb")
		
		this.container.appendChild(this.background);
    this.container.appendChild(this.slider);
    this.parent.appendChild(this.container);

		this.top = this.container.offsetTop;
		this.bottom = this.top + (this.container.offsetHeight - (this.slider.offsetHeight + 2));
		this.domain = this.bottom - this.top;
		this.mult = this.range / this.domain;

		this.slider.addEventListener("mousedown", e => this.slide(e));
  }

  set_value(val){
    this.value = val;
		this.set_position(val);
  }

	set_position(val){
		let v = val - this.range_list[0];
		let p = v / this.mult;
		let y = this.bottom - p;
		this.slider.style.top = y + "px";
		this.adjust_background(y);
		this.oninput(val);
	}

	animate(val){
		this.next = Math.trunc(val);
		this.dir = this.next > this.value ? 0.7 : -0.7;
		const animate = (ts) => {
			this.running = true;
			if(Math.trunc(Math.floor(this.value - this.next)) != 0){
				this.set_value(this.value+= this.dir);
      }
      requestAnimationFrame(animate);
		}
    if(!this.running) requestAnimationFrame(animate);
	}

	adjust_background(pos){
		let val = this.mult * Math.abs(this.bottom - pos);
		let percentage = ((val / this.range) * 90) + 5;
		this.background.setAttribute("style",  "height: " + percentage + "%;");
	}

	slide(e){
		let self = this;
		e = e || window.event;
    e.preventDefault();
		let el = e.target;
		let start_y = e.clientY;
		
		const stop = (e) => {
			document.removeEventListener("mousemove", move);
			document.removeEventListener("mouseup", stop);
		}	
		
		const move = (e) => {
			let move = start_y - e.clientY;
			start_y = e.clientY;
			let pos = (el.offsetTop - move);
			if(pos < this.top || pos > this.bottom) return;
			let val = (this.mult * Math.abs(this.bottom - pos)) + this.range_list[0];
			this.value = val;
			this.oninput(val);
			this.adjust_background(pos);
			el.style.top = pos + "px";
		}

		document.addEventListener("mousemove", move);
		document.addEventListener("mouseup", stop);

	}

	
}
