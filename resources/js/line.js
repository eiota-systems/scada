class Line {
	constructor(color_pallete, parent_id) {
		this.id = parent_id;
		this.parent = document.getElementById(parent_id);
		this.height = this.parent.offsetHeight - 5;
		this.width = this.parent.offsetWidth;
		this.data = [];
		
		this.color_pallete = color_pallete;
		
		this.color = d => this.color_pallete[d];

		this.svg = d3.create("svg")
			.attr("style", "max-height: "+this.height+"px;")
      .attr("viewBox", [0, 0, this.width, this.height])
      .style("overflow", "hidden");
		
		this.parent.appendChild(this.svg.node())
	}
	
	render(max) {
		d3.selectAll("#" + this.id +" svg > *").remove();
		let x = d3.scaleLinear()
      .domain([0, max-1])
      .range([0, this.width]);

		let y = d3.scaleLinear()
      .domain([0, 50])
      .range([0, this.height]);

		let line = d3.line()
      .defined(d => !isNaN(d))
      .x((d, i) => x(i))
      .y((d) => y(d))
			.curve(d3.curveCatmullRom.alpha(0.5));

  	this.svg.append("g")
      .attr("fill", "none")
      .selectAll("path")
      .data(this.data)
      .join("path")
        .attr("d", d => line(d.values))
				.style("stroke", d => this.color(d.key))
	}
	
	append(data) {
		this.data = this.chop_data(this.data, data, this.max);
	}
	
	animate(max) {
		this.max = max;
		let self = this;
		let last = 0;
		const do_animate = (ts) => {
			if(ts - last < 40) {
				requestAnimationFrame(do_animate);
				return;
			}
			last = ts;
			let app = [];
			self.data.forEach((series) => {
				let n = series.values.slice(series.values.length - 1, series.values.length);
				app.push({key: series.key, values: n})
			})
			self.data = self.chop_data(self.data, app, self.max)
			self.render(self.max);
			requestAnimationFrame(do_animate);
		};
		requestAnimationFrame(do_animate);
	}

	chop_data(current, data, max) {
		let sc = 0;
		let final = [];
		data.forEach((series) => {
			final[sc] = {key: sc, values: []}
			current[sc] = current[sc] || {key: sc, values: []};
			let new_data = current[sc].values.concat(series.values);
			let diff = new_data.length - max;
			let d = diff > 0 ? new_data.slice(diff, new_data.length) : new_data;
			final[sc].values = d;
			sc++;
		});
		return final;	
	}	
}
