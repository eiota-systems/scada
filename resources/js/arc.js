class Arc {
  constructor(color_pallete, background_color, domain_max, range_degrees, bar_width, radius, parent) {
		this.background_color = background_color;
		this.color_pallete = color_pallete;
		this.parent_id = parent;
		this.parent = document.getElementById(parent);
		this.domainMax = domain_max;
    this.rangeMaxInDegrees = range_degrees;
    this.barWidth = bar_width;
    this.radius = radius;
		this.current_val = 0;

		const width = this.parent.offsetWidth;
		const radians = Math.PI * 2;
    const range = (this.rangeMaxInDegrees / 360) * radians;
    const height = width;
    const self = this;
		this.tau = radians;
		this.scale = d3.scaleLinear()
    	.domain([0, this.domainMax])
			.range([0, 1])

    this.color = val => {
			return this.color_pallete(val);
		}

		this.arc = d3.arc()
      .innerRadius(this.radius - this.barWidth)
      .outerRadius(this.radius)
      .startAngle(d => self.scale(0))
 
    this.svg = d3.create("svg")
			.attr("style", "max-height: "+height+"px;")
    	.attr("viewBox", [-width/2, -height/2, width, height]);
   	
		this.svg.append("path")
			.datum({endAngle: this.scale(this.domainMax) * radians})
  	  .attr("d", this.arc)
    	.style("fill", this.background_color);
		
		this.foreground = this.svg.append("path")
			.datum({endAngle: 0.42 * radians})
    	.attr("d", this.arc)
      .style("fill", this.color(this.scale(42) * this.tau));

  	this.parent.appendChild(this.svg.node());
	}

	render(val) {
		let self = this;
		const arcTween = (newAngle) => {
			return (d) => {
				let interpolate = d3.interpolate(d.endAngle, newAngle);
				return (t) => {
					d.endAngle = interpolate(t);
					return self.arc(d);
				}
			}
		}
		this.foreground
			.transition()
      .duration(200)
      .style("fill", this.color(this.scale(val) * this.tau))
			.attrTween("d", arcTween(this.scale(val) * this.tau));
	}
}
