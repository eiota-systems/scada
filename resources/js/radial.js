async function radial(parent_id) {
  let data_url1 = "https://gist.githubusercontent.com/veltman/0dbb5dbf33305ce95e84e6a852fa18ce/raw/e19ce18355e5f3ea6a8486758879f26f8540475f/ord-temperature.csv";
  let data_url2 = "https://gist.githubusercontent.com/mbostock/a68c07a2faada9ca47de90886d91deb9/raw/779b2c91d4699c0ab961e1b021049cb80d7292a9/sfo-temperature.csv";
  let loader = Promise.all([data_url1, data_url2].map(file => d3.csv(file, d3.autoType)));
  let raw_data = await loader;
  let data = raw_data.map(list => Array.from(d3.rollup(
    list, 
    v => ({
      date: new Date(Date.UTC(2000, v[0].DATE.getUTCMonth(), v[0].DATE.getUTCDate())),
      avg: d3.mean(v, d => d.TAVG || NaN),
      min: d3.mean(v, d => d.TMIN || NaN),
      max: d3.mean(v, d => d.TMAX || NaN)
    }), 
    d => `${d.DATE.getUTCMonth()}-${d.DATE.getUTCDate()}`).values())
    .sort((a, b) => d3.ascending(a.date, b.date))
  );

  let parent = document.getElementById(parent_id);
  parent.setAttribute("style", "max-height: " + parent.offsetHeight + "px");

  let width = parent.offsetHeight;
  let height = width;
  let margin = 10;

  let outerRadius = width / 2 - margin;

  let innerRadius = width / 5;

  let y = d3.scaleLinear()
    .domain([d3.min(d3.merge(data), d => d.min), d3.max(d3.merge(data), d => d.max)])
    .range([innerRadius, outerRadius])

  let x = d3.scaleUtc()
    .domain([Date.UTC(2000, 0, 1), Date.UTC(2001, 0, 1) - 1])
    .range([0, 2 * Math.PI])
  
  
  let area = d3.areaRadial()
    .curve(d3.curveLinearClosed)
    .defined(d => !isNaN(d.min) && !isNaN(d.max))
    .angle(d => x(d.date))
    .innerRadius(d => y(d.min))
    .outerRadius(d => y(d.max))

  let line = d3.lineRadial()
    .curve(d3.curveLinearClosed)
    .defined(d => !isNaN(d.avg))
    .angle(d => x(d.date))
    .radius(d => y(d.avg))

  let yAxis = g => g
    .attr("text-anchor", "middle")
    .attr("font-family", "sans-serif")
    .attr("font-size", 10)
    .call(g => g.selectAll("g")
      .data(y.ticks(5).reverse())
      .join("g")
        .attr("fill", "none")
        .call(g => g.append("circle")
            .attr("stroke", "#FFF")
            .attr("stroke-opacity", 0.2)
            .attr("r", y))
        .call(g => g.append("text")
            .attr("y", d => -y(d))
            .attr("dy", "0.35em")
            .attr("fill", "#ffffff")
            .text((x, i) => `${x.toFixed(0)}${i ? "" : "°F"}`)
          .clone(true)
            .attr("y", d => y(d))
          .selectAll(function() { return [this, this.previousSibling]; })
          .clone(true)
            .attr("fill", "currentColor")
            .attr("stroke", "none")))
  
  let xAxis = g => g
    .attr("font-family", "sans-serif")
    .attr("font-size", 10)
    .call(g => g.selectAll("g")
      .data(x.ticks())
      .join("g")
        .each((d, i) => d.id = "month"+i)
        .call(g => g.append("path")
            .attr("stroke", "#000")
            .attr("stroke-opacity", 0.2)
            .attr("d", d => `
              M${d3.pointRadial(x(d), innerRadius)}
              L${d3.pointRadial(x(d), outerRadius)}
            `))
        .call(g => g.append("path")
            .attr("id", d => d.id.id)
            .datum(d => [d, d3.utcMonth.offset(d, 1)])
            .attr("fill", "none")
            .attr("d", ([a, b]) => `
              M${d3.pointRadial(x(a), innerRadius)}
              A${innerRadius},${innerRadius} 0,0,1 ${d3.pointRadial(x(b), innerRadius)}
            `))
        .call(g => g.append("text")
          .append("textPath")
            .attr("startOffset", 6)
            .attr("xlink:href", d => d.id.href)
            .text(d3.utcFormat("%B"))))
  
    const svg = d3.create("svg")
      .attr("viewBox", [-width / 2, -height / 2, width, height])
      .attr("style", "max-height: "+ height+"px")
      .attr("stroke-linejoin", "round")
      .attr("stroke-linecap", "round");

    const colors = ["tomato", "steelblue"];
    
    const airports = svg.selectAll("g")
        .data(data)
        .enter()
        .append("g");

    airports.append("path")
        .attr("fill", (d,i) => colors[i])
        .attr("fill-opacity", 0.2)
        .attr("d", area);

    airports.append("path")
        .attr("fill", "none")
        .attr("stroke", (d,i) => colors[i])
        .attr("stroke-width", 1.5)
        .attr("d", line);

    svg.append("g")
        .call(xAxis);

    svg.append("g")
        .call(yAxis);

  parent.appendChild(svg.node());
  return svg;

}
