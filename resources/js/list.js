class List {
  constructor(parent, className, max  = 10){
    this.parent = document.getElementById(parent);
    this.max = max;
    this.items = [];
    this.ul = document.createElement('ul');
    //this.ul.classList.add('list', className);
    this.parent.appendChild(this.ul);
  }

  render(new_items) {
    this.items = new_items.concat(this.items).slice(0, this.max)
		this.ul.innerHTML = "";
    this.items.forEach(item => {
      this.ul.appendChild(item.render(item.data));
    });
  }
}

const render_alert = (item) => {
  let li = document.createElement('li');
  li.classList.add('list-item-alert', 'three-rows');
  let header = document.createElement('div');
  header.classList.add('list-title');
  header.innerHTML = item.title
  let body = document.createElement('div');
  body.classList.add('body');
  body.innerHTML = item.body
  let footer = document.createElement('div');
  footer.classList.add('footer');
	footer.innerHTML = item.footer;
  li.appendChild(header);
  li.appendChild(body);
  li.appendChild(footer);
  return li;
}
