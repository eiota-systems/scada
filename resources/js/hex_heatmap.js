class HexHeatmap {
  constructor(rows, columns, color_pallete, min, max, parent){
    this.rows = rows;
		this.columns = columns;
		this.color_pallete = color_pallete;
    this.parent = document.getElementById(parent);
    this.orig_height = this.parent.offsetHeight - 5;
    this.orig_width = this.parent.offsetWidth;
		this.SQRT3 = Math.sqrt(3);
		this.scale = d3.scaleLinear().domain([min, max])
		this.color = val => this.color_pallete(this.scale(val));
		this.hex_radius = Math.floor(d3.min([this.orig_width/((columns + 0.5) * this.SQRT3), this.orig_height/((rows + 1/3) * 1.5)]))
		this.hexbin = d3.hexbin()
      .radius(this.hex_radius)
      .x(d => d.x)
      .y(d => d.y)

		this.width = Math.ceil(columns * this.hex_radius * this.SQRT3 + this.hex_radius)
		this.height = Math.ceil(rows * 1.5 * this.hex_radius + 0.5 * this.hex_radius) 
    
    this.svg = d3.create("svg")
			.attr("style", "max-height: " + this.orig_height +"px;")
    	.attr("viewBox", [0, 0, this.width, this.height])
		
		this.g = this.svg.append("g").attr("transform", `translate(${this.hex_radius},${this.hex_radius})`)
		this.parent.appendChild(this.svg.node());
  }

	render(points){
		this.g.selectAll(".hexagon")
    	.data(this.hexbin(points))
      .join("path")
      .attr("class", "hexagon")
      .attr("d", d => "M" + d.x + "," + d.y + this.hexbin.hexagon())
      .style("stroke", "rgba(0, 0, 0, 0)")
      .style("stroke-width", 1)
			.transition()
			.duration(500)
	    .style("fill", (d) => this.color(d[0].val))
	}

	points() {
  	let points = []
    for (var i = 0; i < this.rows; i++) {
      for (var j = 0; j < this.columns; j++) {
        let x = this.hex_radius * j * this.SQRT3
        if(i%2 === 1) x += (this.hex_radius * this.SQRT3)/2
              //                  
        let y = this.hex_radius * i * 1.5
        points.push({x: x, y: y, val: Math.random() * 100})
      }
    }
		return points;
	}
}
