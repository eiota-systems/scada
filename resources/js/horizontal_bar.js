class HorizontalBar{
  constructor(data, color_pallete, bar_width, max_domain, parent, dir = "right"){
    this.bar_width = bar_width;
    this.max_domain = max_domain;
		this.color_pallete = color_pallete;
    this.direction = dir;
    this.parent = document.getElementById(parent);
    const width = this.parent.offsetWidth;
    const height = this.parent.offsetHeight - 5;
		this.width = width;

		this.yScale = d3.scaleBand()
    	.domain(d3.range(data.length))
      .rangeRound([0, height])
      .paddingInner(0.05);

		this.xScale = d3.scaleLinear()
   		.domain([0, max_domain])
      .range([0, width]);
    
    this.colorScale = d3.scaleLinear()
   		.domain([0, max_domain]);

    this.color = val => this.color_pallete(this.colorScale(val));

		this.svg = d3.create("svg")
			.attr("style", "max-height: "+height+"px;")
      .attr("viewBox", [0, 0, width, height])
      .style("overflow", "hidden");
		const self = this;
		this.svg.selectAll("rect")
      .data(data)
      .enter()
      .append("rect")
      .attr("x", d => this.get_direction(d))
      .attr("y", (d, i) => this.yScale(i))
      .attr("width", d => this.xScale(d))
      .attr("height", this.bar_width)
      .attr("fill", d => this.color(d));

		this.parent.appendChild(this.svg.node());
  }

  get_direction(d) {
    if(this.direction == "right"){
      return this.width - this.xScale(d)
    }else{
      return 0;
    }
  }


  render(data){
		this.svg.selectAll("rect")
    	.data(data)
      .transition()
      .duration(200)
      .attr("x", d => this.get_direction(d))
      .attr("width", d => this.xScale(d))
      .attr("fill", d => this.color(d));
  }
}
