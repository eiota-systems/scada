const load = () => {
  let gauge1 = new Arc(d3.interpolateOranges, "rgba(50, 50, 50, 0.2)", 100, 270, 5, 100, 'gauge-1');
  let gauge2 = new Arc(d3.interpolateOranges, "rgba(50, 50, 50, 0.2)", 100, 270, 5, 92, 'gauge-2');
  let gauge3 = new Arc(d3.interpolateOranges, "rgba(50, 50, 50, 0.2)", 100, 270, 5, 84, 'gauge-3');
  let gauge4 = new Arc(d3.interpolateOranges, "rgba(50, 50, 50, 0.2)", 100, 270, 5, 76, 'gauge-4');
  let gauge5 = new Arc(d3.interpolateOranges, "rgba(50, 50, 50, 0.2)", 100, 270, 5, 68, 'gauge-5');

  let level_1 = new Bar(gen_multi_data(1), d3.interpolateOranges, 100, 100, 'level-1');
  let level_2 = new Bar(gen_multi_data(1), d3.interpolateOranges, 100, 100, 'level-2');
  let level_3 = new Bar(gen_multi_data(1), d3.interpolateTurbo, 140, 100, 'level-3');

  let bar = new Bar(gen_multi_data(25), d3.interpolateTurbo, 5, 100, 'multi');
  let hor_bar_right = new HorizontalBar(gen_multi_data(50), d3.interpolateBlues, 5, 100, 'horizontal-right', "right");
  let hor_bar_left = new HorizontalBar(gen_multi_data(50), d3.interpolateReds, 5, 100, 'horizontal-left', "left");
	let hor_bar_3 = new HorizontalBar(gen_multi_data(30), d3.interpolateBlues, 5, 100, 'hor-bar-3', "right");

  let slider1 = new Slider([0, 110], 'slider-1', (value) => document.getElementById('slider-1-value').innerHTML = value.toFixed(2), true);
  let slider2 = new Slider([0, 300], 'slider-2', (value) => document.getElementById('slider-2-value').innerHTML = value.toFixed(2), true);
  let slider3 = new Slider([10, 150], 'slider-3', (value) => document.getElementById('slider-3-value').innerHTML = value.toFixed(2), true);
  let slider4 = new Slider([10, 200], 'slider-4', (value) => document.getElementById('slider-4-value').innerHTML = value.toFixed(2), true);
	
	slider1.set_value(50);
	slider2.set_value(100);
	slider3.set_value(20);
	slider4.set_value(100);

  let line = new Line(d3.schemeDark2, 'spline');

	let alert_list_1 = new List('alert-list-1', 'alert-list', 6);
	let alert_list_2 = new List('alert-list-2', 'alert-list', 3);
	  
  let process = new Sankey(process_data, 'process');

  let rad = radial('radial');

	let counter = new Counter('alert-2');
  let counter1 = new Counter('counter1');
  let counter2 = new Counter('counter2');
  let counter3 = new Counter('counter3');

	let heatmap_1 = new HexHeatmap(7, 20, d3.interpolateBlues, 0, 100, 'heatmap-1');
	let heatmap_2 = new HexHeatmap(15, 6, d3.interpolateOranges, 0, 100, 'heatmap-2');
	
	heatmap_1.render(heatmap_1.points());
	heatmap_2.render(heatmap_2.points());

	toggle1 = new Toggle('A', (e) => console.log("toggle clicked"), 'toggle-1');
	toggle2 = new Toggle('B', (e) => console.log("toggle clicked"), 'toggle-2');
	toggle3 = new Toggle('C', (e) => console.log("toggle clicked"), 'toggle-3');
	toggle4 = new Toggle('D', (e) => console.log("toggle clicked"), 'toggle-4');

	counter.render(Math.random()*100);
	counter1.render(Math.random()*100);
	counter2.render(Math.random()*100);
	counter3.render(Math.random()*100);
	line.animate(30);
	alert_list_1.render(get_alert_items());
	alert_list_2.render(get_alert_items());
  
  animate_process([process])
	animate_slider([slider1, slider2, slider3, slider4])
	animate_heatmap([heatmap_1, heatmap_2]);
  animate_level([level_1, level_2, level_3]);
	animate_counter([counter, counter1, counter2, counter3]);
	animate_list([alert_list_1, alert_list_2]);
  animate_radial([gauge1, gauge2, gauge3, gauge4, gauge5]);
  animate_multi([bar]);
	animate_hor([hor_bar_right, hor_bar_left, hor_bar_3]);
  animate_line([line]);
	animate_alert_panels([document.getElementById("alert-2")]);
}

const animate_process = (charts) => {
  setInterval(() => {
    charts.forEach(chart => {
      let id = Math.floor(Math.random()*process_data.nodes.length);
      let name = process_data.nodes[id].name;
      let source = document.getElementById(chart.name_to_id(name));
      let source_links = document.getElementsByClassName(chart.name_to_id(name) + "-link");
      source.classList.toggle('process-highlight');
      for(let link of source_links){
        link.classList.toggle('process-highlight');
      }
    })
  }, 300)
}
const animate_alert_panels = (panels) => {
	setInterval(() => {
		panels.forEach(panel => {
			panel.classList.toggle('alert-panel');
			panel.classList.toggle('alert-panel-reverse');
		})
	}, 9000)
}

const get_alert_items = () => {
	let titles = ["VOC Levels", "Chiller Status", "PM levels", "CO2 Levels"];
	return [{data:{title: rand_el(titles), body: (Math.random()*30).toFixed(3), footer: "ACK"}, render: render_alert}];
}

const rand_el = (list) => {
	let el = Math.ceil(Math.random() * (list.length - 1));
	return list[el];
}

const gen_line_data = (amount = 17) => {
  let data = [{key: 0, values: []}, {key: 1, values: []}, {key: 2, values: []}, {key: 3, values: []}, {key: 4, values: []}, {key: 5, values: []}, {key: 6, values: []}, {key: 7, values: []}];
	let sc = 0;
  data.forEach(series => {
    [...Array(amount).keys()].forEach(id => {
			let choice = Math.floor(Math.random() * data.length);
			if(choice == sc) {
				series.values.push(Math.random()*45);
			}
    });
		sc++;
  });
  return data;
}
const gen_multi_data = (amount) => {
	let data = [];
  [...Array(amount).keys()].forEach(id => data.push(Math.random()*80));
	return data;
}

const animate_slider = (charts) => {
	setInterval(() => charts.forEach(chart => chart.animate((Math.random()*100) + 10)), 1000)
}

const animate_heatmap = (charts) => {
	setInterval(() => charts.forEach(chart => chart.render(chart.points())), 700)
}

const animate_counter = (charts) => {
	setInterval(() => charts.forEach(chart => chart.render((Math.random()*100))), 5000)
}

const animate_list = (charts) => {
  setInterval(() => charts.forEach(chart => chart.render(get_alert_items())), 900);
}

const animate_line = (charts) => {
  setInterval(() => charts.forEach(chart => chart.append(gen_line_data(1))), 600);
}

const animate_multi = (charts) => {
  setInterval(() => charts.forEach(chart => chart.render(gen_multi_data(25))), 500);
}

const animate_level = (charts) => {
  setInterval(() => charts.forEach(chart => chart.render(gen_multi_data(1))), 500);
}

const animate_hor = (charts) => {
  setInterval(() => charts.forEach(chart => chart.render(gen_multi_data(50))), 400);
}
const animate_radial = (charts) => {
  setInterval(() => charts.forEach(chart => chart.render(Math.random()*100)), 800);
}
