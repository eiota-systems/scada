class Toggle{
  constructor(text, callback, parent){
    this.parent = document.getElementById(parent);
    this.height = this.parent.offsetHeight;

    this.container = document.createElement("div");
    this.container.classList.add("toggle");

    this.led = document.createElement("div");
    this.led.classList.add("led");

    this.button = document.createElement("div");
    this.button.classList.add("button");

    this.text = document.createElement("span");
    this.text.innerHTML = text;
    this.button.appendChild(this.text);

    this.container.appendChild(this.led);
    this.container.appendChild(this.button)
    this.container.addEventListener("click", e => this.do_click(e, callback), false);
    this.parent.appendChild(this.container);
  }

  do_click(e, callback){
    callback(e);
    this.toggle();
  }

  toggle(){
    this.container.classList.toggle("on");
    this.container.classList.toggle("off");
  }
}
