class Sankey {
  constructor(data, parent){
    this.parent = document.getElementById(parent);
    this.width = this.parent.offsetWidth;
    this.height = this.parent.offsetHeight - 5;
    this.data = data;
		const edgeColor = "input";
		this.colors = d3.scaleOrdinal(d3.schemeCategory10);
    this.color = d => this.colors(d.category === undefined ? d.name : d.category);
		this.format = d3.format(",.0f");
		this.do_format = d => this.data.units ? d => `${this.format(d)} ${this.data.units}` : this.format;

		this.svg = d3.create("svg")
			.attr("style", "max-height: " + this.height +"px;")
    	.attr("viewBox", [0, 0, this.width, this.height]);
		
    const {nodes, links} = this.get_sankey(this.data.nodes, this.data.links);

    this.svg.append("g")
    		.attr("stroke", "#000")
      .selectAll("rect")
      .data(nodes)
      .join("rect")
      	.attr("x", d => d.x0)
        .attr("y", d => d.y0)
        .attr("height", d => d.y1 - d.y0)
        .attr("width", d => d.x1 - d.x0)
        .attr("fill", this.color)
        .attr("id", d => this.name_to_id(d.name))
      .append("title")
      	.text(d => `${d.name}\n${this.format(d.value)}`);

    const link = this.svg.append("g")
    	.attr("fill", "none")
      .attr("stroke-opacity", 0.5)
      .selectAll("g")
      	.data(links)
        .join("g")
        //.style("mix-blend-mode", "multiply");

    link.append("path")
    	.attr("d", d3.sankeyLinkHorizontal())
      .attr("stroke", d => edgeColor === "none" ? "#aaa"
      	: edgeColor === "path" ? d.uid 
        : edgeColor === "input" ? this.color(d.source) 
        : this.color(d.target))
      .attr("stroke-width", d => Math.max(1, d.width))
      .attr("class", d => this.name_to_id(d.source.name) + "-link");

    link.append("title")
    	.text(d => `${d.source.name} → ${d.target.name}\n${this.format(d.value)}`);

    this.svg.append("g")
    	.attr("font-family", "monospace")
			.attr("fill", "rgb(255, 255, 255)")
      .attr("font-size", 10)
      .selectAll("text")
        .data(nodes)
        .join("text")
          .attr("x", d => d.x0 < this.width / 2 ? d.x1 + 6 : d.x0 - 6)
          .attr("y", d => (d.y1 + d.y0) / 2)
          .attr("dy", "0.35em")
          .attr("text-anchor", d => d.x0 < this.width / 2 ? "start" : "end")
          .text(d => d.name);

    this.parent.appendChild(this.svg.node());
  }

	get_sankey(nodes, links) {
  	const sankey = d3.sankey()
    	.nodeId(d => d.name)
      .nodeAlign(d3["sankeyLeft"])
      .nodeWidth(15)
      .nodePadding(10)
      .extent([[1, 5], [this.width - 1, this.height - 5]]);
		return sankey({
    	nodes: nodes.map(d => Object.assign({}, d)),
      links: links.map(d => Object.assign({}, d))
    });
  }

  name_to_id(name){ 
    return name.replace(" ", "-").replace("'", "").replace(".", "-").toLowerCase();
  }
}
