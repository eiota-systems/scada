# Release step
FROM nginx:1.17.3-alpine

COPY index.html /usr/share/nginx/html/index.html
COPY ./resources /usr/share/nginx/html/resources
COPY ./nginx/default.conf /etc/nginx/conf.d/default.conf
CMD nginx -g 'daemon off;'
